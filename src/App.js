import React, {useState} from 'react';

import { If, Then, Else } from 'react-if';

import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Spinner from 'react-bootstrap/Spinner'

import TagDisplay from './TagDisplay'

const SGTIN96 = 'SGTIN-96';

const wsUrl = 'wss://websocket.ericbetts.dev';
const ws = new WebSocket(wsUrl);

ws.onopen = (event) => {
  ws.send(JSON.stringify({
    action: 'subscribe',
    key: '5cea14d98f2f008279c8fbe1cd80de2e60f7794675f779f0c2d617cb8b159efd',
  }));

  setInterval(() => {
    ws.send(JSON.stringify({
      action: 'echo',
      value: (new Date()).getTime(),
    }));
  }, 5 * 60 * 1000);
};


function App() {
  const [tags, setTags] = useState([]);
  const [subscribed, setSubscribed] = useState(false);

  ws.onmessage = (message) => {
    const { data } = message;
    try {
      const parsed = JSON.parse(data);
      const { action, state } = parsed;
      // Ignore echo messages
      if (action && action === 'echo') {
        return;
      }
      //Ignore subscribed message
      if (state && state === 'subscribed') {
        setSubscribed(true);
        return;
      }
      const products = parsed.tags.filter(tag => {
        console.log(tag)
        return tag.CodingScheme === SGTIN96;
      })
      setTags([...tags, ...products]);
    } catch (e) {
      // Not JSON
      console.log(data);
    }
  };

  return (
    <>
      <Navbar>
        <Navbar.Brand>Ad Wall</Navbar.Brand>
        <Nav>
        <Nav.Link href="">
          TODO: Sourcecode (GitLab)
        </Nav.Link>
        </Nav>
      </Navbar>
      <Container>
        <Row>
          <Col>
            <Jumbotron>
              <h1>Ad Wall</h1>
              <p>Show a product based on scanning a UHF RFID tag</p>
            </Jumbotron>
          </Col>
        </Row>
        <Row>
          <Col lg={12}>
            <Card>
              <Card.Header>Products</Card.Header>
              <Card.Body>
                <If condition={subscribed}>
                  <Then>
                    <Card.Title>Products</Card.Title>
                    <ListGroup>
                      {tags.map((tag, index) => {
                        return (
                          <ListGroup.Item key={index}>
                            <TagDisplay tag={tag} />
                          </ListGroup.Item>
                        );
                      })}
                    </ListGroup>
                  </Then>
                  <Else>
                    <Spinner animation="border" role="status">
                      <span className="sr-only">Loading...</span>
                    </Spinner>
                  </Else>
                </If>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
      <footer className="page-footer font-small pt-5 mt-5 mx-auto">
        <Container fluid className="text-center">
          <Row noGutters>
            <Col>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
}

export default App;
