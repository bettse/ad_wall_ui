import React, {useEffect, useRef} from 'react';
import bwipjs from 'bwip-js';

const DECIMAL = 10;

//{'EPC-96': '30340110c437b8806a600074', 'ROSpecID': (1,), 'SpecIndex': (1,), 'InventoryParameterSpecID': (1,), 'AntennaID': (1,), 'PeakRSSI': (-65,), 'ChannelIndex': (36,), 'FirstSeenTimestampUTC': (1615434008743000,), 'LastSeenTimestampUTC': (1615434008743000,), 'TagSeenCount': (1,), 'AccessSpecID': (0,), 'CodingScheme': 'SGTIN-96', 'URI': 'urn:epc:tag:sgtin-96:1.0017457.057058.1784676468', 'parts': {'header': 48, 'filter': 1, 'partition': '101', 'company_prefix': '0017457', 'item_reference': '057058', 'serial': 1784676468}, 'gcp': {'code': '0017459000000', 'name': 'Haggar Clothing Company', 'address_1': '11511 Luna Rd', 'address_2': '', 'address_3': 'TX', 'cp': '75234', 'city': 'Dallas', 'country': 'US'}}

function TagDisplay(props) {
	const { tag } = props;
	const { CodingScheme, URI, parts, gcp, gtin } = tag;
	const { company_prefix, item_reference, serial } = parts;
  const canvasRef = useRef(null)

  useEffect(() => {
    // Strip leading 0's
    const cp = parseInt(company_prefix, DECIMAL).toString();
    const ir = parseInt(item_reference, DECIMAL).toString();

    const check_digit = calcCheckDigit(cp + ir);
    const upc = cp + ir + check_digit;

    try {
      bwipjs.toCanvas(canvasRef.current, {
        bcid:        'upca',       // Barcode type
        text:        upc,    // Text to encode
        scale:       3,               // 3x scaling factor
        height:      10,              // Bar height, in millimeters
        includetext: true,            // Show human-readable text
        textxalign:  'center',        // Always good to set this
      });
    } catch (e) {
      console.error(e)
      // `e` may be a string or Error object
    }
  });

	return (
		<>
		{CodingScheme} {URI}
		<p>Company Prefix: <span>{company_prefix}</span>
		<span>{gcp?.name}</span>
		</p>
		<p>Item Reference: <span>{item_reference}</span>
		{gtin?.name}
		</p>
		<p>Serial Number: <span>{serial}</span></p>
		<canvas ref={canvasRef}></canvas>
		</>
	);
}

function calcCheckDigit(code) {
  let check = 0
  code = code.toString()

  while(code.length < 11) { code = "0" + code }

  for(let i = 0; i < code.length; i += 2) {
    check += parseInt(code.charAt(i))
  }

  check *= 3;

  for(let i = 1; i < code.length; i += 2) {
    check += parseInt(code.charAt(i))
  }

  check %= 10;
  check = (check === 0) ? check : 10 - check

  return check
}

export default TagDisplay;
